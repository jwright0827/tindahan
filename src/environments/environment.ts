// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCQbm4JwLPGcyE9thdAogujLKaGkDer4RA',
    authDomain: 'liko-2609m.firebaseapp.com',
    databaseURL: 'https://liko-2609m.firebaseio.com',
    projectId: 'liko-2609m',
    storageBucket: 'liko-2609m.appspot.com',
    messagingSenderId: '623370472860'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
