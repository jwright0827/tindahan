import { LoginComponent } from './../login/login.component';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { AuthService } from '../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { SignupComponent } from '../signup/signup.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})

export class BsNavbarComponent {
  signtype: string;
  // tslint:disable-next-line:max-line-length
  imagePath = 'https://firebasestorage.googleapis.com/v0/b/liko-2609t.appspot.com/o/images%2FLikoLogo.png?alt=media&token=06ed98dd-64e6-4a79-b2dd-877d5df2bfa0';
  constructor(public auth: AuthService, private dialog: MatDialog) {
  }


  openDialog() {
    if (this.signtype === 'signup') {
      this.dialog.open(SignupComponent)
        .afterClosed()
        .subscribe(result => console.log(result));
    } else {
      this.dialog.open(LoginComponent)
        .afterClosed()
        .subscribe(result => console.log(result));
    }
  }
  logout() {
    this.auth.logout();
  }

}
