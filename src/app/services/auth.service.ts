import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class AuthService {
  user$: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth) {
    this.user$ = afAuth.authState;
  }

  loginwithGoogle() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then(function(result) {
      console.log(result);
      console.log('Login with google Successfully');
    });
  }

  loginwithFacebook() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then(function(result) {
      console.log('Login with facebook Successfully');
    });
  }
  signUpWithEmail(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(function(result) {
      console.log('Sign up Successfully');
    })
    .catch(function(error) {
      console.log('Error in SignUpWithEmail : ' + error);
    });
  }
  loginwithEmail(email: string, password: string): boolean {
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
    .then(function(result) {
      console.log('Logged on user : ' + JSON.stringify(result));
      return true;
    })
    .catch(function(error) {
      console.log(error);
      return false;
    });
    return true;
  }
  logout() {
    this.afAuth.auth.signOut()
    .then(function() {
    console.log('Sign-out successful.');
    }).catch(function(error) {
    console.log('error' + error);
    });
  }
  registerOperator() {
  }
}
