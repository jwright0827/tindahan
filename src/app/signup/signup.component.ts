import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationErrors, AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  constructor(private auth: AuthService) {
  }
  locations = [
    { id: 1, name: 'Tagbilaran' },
    { id: 2, name: 'Tubigon' },
  ];
  form = new FormGroup({
    operatorName: new FormControl('', Validators.required),
    telephoneNo: new FormControl('', Validators.required),
    mobileNo: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    businessName: new FormControl('', Validators.required)
  });


  static isValid(control: AbstractControl): Promise<ValidationErrors|null> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'jwright') {
          resolve({ shouldBeUnique: true });
        } else {
          resolve(null);
        }
      }, 2000);
    });
}
  register() {
    // this.auth.signUpWithEmail(this.form.value)
  }

}
