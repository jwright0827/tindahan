import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  locations = [
    { id: 1, name: 'Tagbilaran' },
    { id: 2, name: 'Tubigon' },
  ];
  form = new FormGroup({
    operatorName: new FormControl('', Validators.required),
    telephoneNo: new FormControl('', Validators.required),
    mobileNo: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    businessName: new FormControl('', Validators.required)
  });
  get operatorName() {
    return this.form.get('operatorName');
  }
  get telephoneNo() {
    return this.form.get('telephoneNo');
  }
  get mobileNo() {
    return this.form.get('mobileNo');
  }
  get location() {
    return this.form.get('location');
  }
  get businessName() {
    return this.form.get('businessName');
  }
  close() { window.close(); }
  register() { }
}
