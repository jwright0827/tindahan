import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { BsNavbarComponent } from './bs-navbar/bs-navbar.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from './services/auth.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule, MatTooltip } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { SignupComponent } from './signup/signup.component';
import { ExploreComponent } from './explore/explore.component';
import { ItenaryComponent } from './itenary/itenary.component';
import { MybookingsComponent } from './mybookings/mybookings.component';
import { InboxComponent } from './inbox/inbox.component';
import { ProfileComponent } from './profile/profile.component';
import { VehicleregistrationComponent } from './operator/vehicleregistration/vehicleregistration.component';
import { BookingmanagementComponent } from './operator/bookingmanagement/bookingmanagement.component';
import { ConfigurationComponent } from './operator/configuration/configuration.component';
import { ModalComponent } from './modal/modal.component';
import { RegistrationComponent } from './operator/registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    ExploreComponent,
    ItenaryComponent,
    MybookingsComponent,
    InboxComponent,
    ProfileComponent,
    VehicleregistrationComponent,
    BookingmanagementComponent,
    ConfigurationComponent,
    ModalComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: HomeComponent},
      { path: 'login', component: LoginComponent},
      { path: 'signup', component: SignupComponent},
      { path: 'explore', component: ExploreComponent},
      { path: 'itenary', component: ItenaryComponent},
      { path: 'mybookings', component: MybookingsComponent},
      { path: 'inbox', component: InboxComponent},
      { path: 'profile', component: ProfileComponent},
      { path: 'operator/vehicleregistration', component: VehicleregistrationComponent},
      { path: 'operator/bookingmanagement', component: BookingmanagementComponent},
      { path: 'operator/configuration', component: ConfigurationComponent}
    ])
  ],
  entryComponents: [
    LoginComponent,
    SignupComponent
  ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
