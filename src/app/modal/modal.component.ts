import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;

  constructor() { }

  ngOnInit() {
  }
  ngOnDestroy(): void {
  }
}
