import { AuthService } from '../services/auth.service';
import { Component, OnInit } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  email = 'email@example.com';
  password = 'enter password';
  constructor(
    private route: ActivatedRoute,
    private auth: AuthService) {
  }

  ngOnInit() {
    // this.loginForm = this.formBuilder.group({
    //     username: ['', Validators.required],
    //     password: ['', Validators.required]
    // });
    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  // convenience getter for easy access to form fields
  // get f() { return this.loginForm.controls; }
  close() {
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  loginwithGoogle() {
    this.auth.loginwithGoogle();
    }
  loginwithFacebook() {
   this.auth.loginwithFacebook();
  }
  signUpWithEmail() {
    this.auth.signUpWithEmail(this.email, this.password);
  }
  loginwithEmail() {
    this.auth.loginwithEmail(this.email, this.password);
  }
}
