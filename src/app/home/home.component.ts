import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  images0 = '../assets/images/car1.png';
  images1 = '../assets/images/car3.png';
  images2 = '../assets/images/car4.png';
  images3 = '../assets/images/car5.png';
  constructor() { }

  ngOnInit() {
  }

}
