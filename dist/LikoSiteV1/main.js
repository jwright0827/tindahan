(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<bs-navbar></bs-navbar>\r\n<router-outlet></router-outlet>\r\n<div class=\"container\">\r\n\r\n</div>\r\n<!-- \r\n<mat-checkbox>Subscribe to new letter</mat-checkbox>\r\n<mat-icon>add_alert</mat-icon> -->"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'LikoSite1';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _bs_navbar_bs_navbar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./bs-navbar/bs-navbar.component */ "./src/app/bs-navbar/bs-navbar.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _explore_explore_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./explore/explore.component */ "./src/app/explore/explore.component.ts");
/* harmony import */ var _itenary_itenary_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./itenary/itenary.component */ "./src/app/itenary/itenary.component.ts");
/* harmony import */ var _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./mybookings/mybookings.component */ "./src/app/mybookings/mybookings.component.ts");
/* harmony import */ var _inbox_inbox_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./inbox/inbox.component */ "./src/app/inbox/inbox.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/profile/profile.component.ts");
/* harmony import */ var _operator_vehicleregistration_vehicleregistration_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./operator/vehicleregistration/vehicleregistration.component */ "./src/app/operator/vehicleregistration/vehicleregistration.component.ts");
/* harmony import */ var _operator_bookingmanagement_bookingmanagement_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./operator/bookingmanagement/bookingmanagement.component */ "./src/app/operator/bookingmanagement/bookingmanagement.component.ts");
/* harmony import */ var _operator_configuration_configuration_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./operator/configuration/configuration.component */ "./src/app/operator/configuration/configuration.component.ts");
/* harmony import */ var _modal_modal_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./modal/modal.component */ "./src/app/modal/modal.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _bs_navbar_bs_navbar_component__WEBPACK_IMPORTED_MODULE_9__["BsNavbarComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_10__["HomeComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_20__["SignupComponent"],
                _explore_explore_component__WEBPACK_IMPORTED_MODULE_21__["ExploreComponent"],
                _itenary_itenary_component__WEBPACK_IMPORTED_MODULE_22__["ItenaryComponent"],
                _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_23__["MybookingsComponent"],
                _inbox_inbox_component__WEBPACK_IMPORTED_MODULE_24__["InboxComponent"],
                _profile_profile_component__WEBPACK_IMPORTED_MODULE_25__["ProfileComponent"],
                _operator_vehicleregistration_vehicleregistration_component__WEBPACK_IMPORTED_MODULE_26__["VehicleregistrationComponent"],
                _operator_bookingmanagement_bookingmanagement_component__WEBPACK_IMPORTED_MODULE_27__["BookingmanagementComponent"],
                _operator_configuration_configuration_component__WEBPACK_IMPORTED_MODULE_28__["ConfigurationComponent"],
                _modal_modal_component__WEBPACK_IMPORTED_MODULE_29__["ModalComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["BrowserAnimationsModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_16__["MatCheckboxModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_4__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].firebase),
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_5__["AngularFirestoreModule"],
                _angular_fire_auth__WEBPACK_IMPORTED_MODULE_7__["AngularFireAuthModule"],
                _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__["AngularFireStorageModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIconModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_18__["MatTooltipModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModule"].forRoot(),
                _angular_router__WEBPACK_IMPORTED_MODULE_11__["RouterModule"].forRoot([
                    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_10__["HomeComponent"] },
                    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"] },
                    { path: 'signup', component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_20__["SignupComponent"] },
                    { path: 'explore', component: _explore_explore_component__WEBPACK_IMPORTED_MODULE_21__["ExploreComponent"] },
                    { path: 'itenary', component: _itenary_itenary_component__WEBPACK_IMPORTED_MODULE_22__["ItenaryComponent"] },
                    { path: 'mybookings', component: _mybookings_mybookings_component__WEBPACK_IMPORTED_MODULE_23__["MybookingsComponent"] },
                    { path: 'inbox', component: _inbox_inbox_component__WEBPACK_IMPORTED_MODULE_24__["InboxComponent"] },
                    { path: 'profile', component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_25__["ProfileComponent"] },
                    { path: 'operator/vehicleregistration', component: _operator_vehicleregistration_vehicleregistration_component__WEBPACK_IMPORTED_MODULE_26__["VehicleregistrationComponent"] },
                    { path: 'operator/bookingmanagement', component: _operator_bookingmanagement_bookingmanagement_component__WEBPACK_IMPORTED_MODULE_27__["BookingmanagementComponent"] },
                    { path: 'operator/configuration', component: _operator_configuration_configuration_component__WEBPACK_IMPORTED_MODULE_28__["ConfigurationComponent"] }
                ])
            ],
            providers: [
                _services_auth_service__WEBPACK_IMPORTED_MODULE_14__["AuthService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/bs-navbar/bs-navbar.component.css":
/*!***************************************************!*\
  !*** ./src/app/bs-navbar/bs-navbar.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".dropdown-toggle{\r\n   cursor: pointer; \r\n}\r\nmat-icon.explore{\r\n   color: red;\r\n   width: 50px;\r\n   height: 50px;\r\n   line-height: 50px;\r\n   -webkit-transform: scale(2);\r\n           transform: scale(2);\r\n}\r\nmat-icon.zoom_out_map{\r\n   color: yellow;\r\n   width: 50px;\r\n   height: 50px;\r\n   line-height: 50px;\r\n   -webkit-transform: scale(2);\r\n           transform: scale(2);\r\n}\r\nmat-icon.assignment{\r\n   color: greenyellow;\r\n   width: 50px;\r\n   height: 50px;\r\n   line-height: 50px;\r\n   -webkit-transform: scale(2);\r\n           transform: scale(2);\r\n}\r\nmat-icon.mail{\r\n   color: blueviolet;\r\n   width: 50px;\r\n   height: 50px;\r\n   line-height: 50px;\r\n   -webkit-transform: scale(2);\r\n           transform: scale(2);\r\n}\r\nmat-icon.person{\r\n   color: blanchedalmond;\r\n   width: 30px;\r\n   height: 30px;\r\n   line-height: 30px;\r\n   -webkit-transform: scale(2);\r\n           transform: scale(2);\r\n}\r\nmat-icon.vpn_key{\r\n   color: blanchedalmond;\r\n   width: 30px;\r\n   height: 30px;\r\n   line-height: 30px;\r\n   -webkit-transform: scale(2);\r\n           transform: scale(2);\r\n}\r\nmat-icon{\r\n   color:purple;\r\n   size: 100;\r\n}\r\n.tooltipred {\r\n   background: #b71c1c;\r\n }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYnMtbmF2YmFyL2JzLW5hdmJhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0dBQ0csZ0JBQWdCO0NBQ2xCO0FBQ0Q7R0FDRyxXQUFXO0dBQ1gsWUFBWTtHQUNaLGFBQWE7R0FDYixrQkFBa0I7R0FDbEIsNEJBQW9CO1dBQXBCLG9CQUFvQjtDQUN0QjtBQUNEO0dBQ0csY0FBYztHQUNkLFlBQVk7R0FDWixhQUFhO0dBQ2Isa0JBQWtCO0dBQ2xCLDRCQUFvQjtXQUFwQixvQkFBb0I7Q0FDdEI7QUFDRDtHQUNHLG1CQUFtQjtHQUNuQixZQUFZO0dBQ1osYUFBYTtHQUNiLGtCQUFrQjtHQUNsQiw0QkFBb0I7V0FBcEIsb0JBQW9CO0NBQ3RCO0FBQ0Q7R0FDRyxrQkFBa0I7R0FDbEIsWUFBWTtHQUNaLGFBQWE7R0FDYixrQkFBa0I7R0FDbEIsNEJBQW9CO1dBQXBCLG9CQUFvQjtDQUN0QjtBQUNEO0dBQ0csc0JBQXNCO0dBQ3RCLFlBQVk7R0FDWixhQUFhO0dBQ2Isa0JBQWtCO0dBQ2xCLDRCQUFvQjtXQUFwQixvQkFBb0I7Q0FDdEI7QUFDRDtHQUNHLHNCQUFzQjtHQUN0QixZQUFZO0dBQ1osYUFBYTtHQUNiLGtCQUFrQjtHQUNsQiw0QkFBb0I7V0FBcEIsb0JBQW9CO0NBQ3RCO0FBRUQ7R0FDRyxhQUFhO0dBQ2IsVUFBVTtDQUNaO0FBQ0Q7R0FDRyxvQkFBb0I7RUFDckIiLCJmaWxlIjoic3JjL2FwcC9icy1uYXZiYXIvYnMtbmF2YmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZHJvcGRvd24tdG9nZ2xle1xyXG4gICBjdXJzb3I6IHBvaW50ZXI7IFxyXG59XHJcbm1hdC1pY29uLmV4cGxvcmV7XHJcbiAgIGNvbG9yOiByZWQ7XHJcbiAgIHdpZHRoOiA1MHB4O1xyXG4gICBoZWlnaHQ6IDUwcHg7XHJcbiAgIGxpbmUtaGVpZ2h0OiA1MHB4O1xyXG4gICB0cmFuc2Zvcm06IHNjYWxlKDIpO1xyXG59XHJcbm1hdC1pY29uLnpvb21fb3V0X21hcHtcclxuICAgY29sb3I6IHllbGxvdztcclxuICAgd2lkdGg6IDUwcHg7XHJcbiAgIGhlaWdodDogNTBweDtcclxuICAgbGluZS1oZWlnaHQ6IDUwcHg7XHJcbiAgIHRyYW5zZm9ybTogc2NhbGUoMik7XHJcbn1cclxubWF0LWljb24uYXNzaWdubWVudHtcclxuICAgY29sb3I6IGdyZWVueWVsbG93O1xyXG4gICB3aWR0aDogNTBweDtcclxuICAgaGVpZ2h0OiA1MHB4O1xyXG4gICBsaW5lLWhlaWdodDogNTBweDtcclxuICAgdHJhbnNmb3JtOiBzY2FsZSgyKTtcclxufVxyXG5tYXQtaWNvbi5tYWlse1xyXG4gICBjb2xvcjogYmx1ZXZpb2xldDtcclxuICAgd2lkdGg6IDUwcHg7XHJcbiAgIGhlaWdodDogNTBweDtcclxuICAgbGluZS1oZWlnaHQ6IDUwcHg7XHJcbiAgIHRyYW5zZm9ybTogc2NhbGUoMik7XHJcbn1cclxubWF0LWljb24ucGVyc29ue1xyXG4gICBjb2xvcjogYmxhbmNoZWRhbG1vbmQ7XHJcbiAgIHdpZHRoOiAzMHB4O1xyXG4gICBoZWlnaHQ6IDMwcHg7XHJcbiAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICB0cmFuc2Zvcm06IHNjYWxlKDIpO1xyXG59XHJcbm1hdC1pY29uLnZwbl9rZXl7XHJcbiAgIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcclxuICAgd2lkdGg6IDMwcHg7XHJcbiAgIGhlaWdodDogMzBweDtcclxuICAgbGluZS1oZWlnaHQ6IDMwcHg7XHJcbiAgIHRyYW5zZm9ybTogc2NhbGUoMik7XHJcbn1cclxuXHJcbm1hdC1pY29ue1xyXG4gICBjb2xvcjpwdXJwbGU7XHJcbiAgIHNpemU6IDEwMDtcclxufVxyXG4udG9vbHRpcHJlZCB7XHJcbiAgIGJhY2tncm91bmQ6ICNiNzFjMWM7XHJcbiB9Il19 */"

/***/ }),

/***/ "./src/app/bs-navbar/bs-navbar.component.html":
/*!****************************************************!*\
  !*** ./src/app/bs-navbar/bs-navbar.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md fixed-top\" style=\"background-color: orange;\">\n  \n  <a class=\"navbar-brand\" routerLink=\"/\">\n    <img [src]=\"imagePath\" \n     width=\"50\" height=\"50\" class=\"d-inline-block align-top\" alt=\"Liko\">\n  </a>\n  <a class=\"navbar-brand\">LIKO</a>\n  <div class=\"navbar-nav mr-auto text-center\">\n      <input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search\" aria-label=\"Search\">\n      <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"submit\">Search</button>\n  </div>\n  <ul class=\"navbar-nav mr-auto\" *ngIf=\"auth.user$ | async as user\">\n      <span matTooltip=\"Explore Liko Transport\" matTooltipClass=\"tooltipred\">    \n        <a  routerLink=\"/explore\" >\n          <li> <mat-icon class='explore'>explore</mat-icon></li>\n        </a>\n      </span>\n      <span matTooltip=\"Liko Itenary\" matTooltipClass=\"tooltipred\">  \n        <a  routerLink=\"/itenary\" >\n          <li> <mat-icon class='zoom_out_map'>zoom_out_map</mat-icon></li>\n        </a>\n      </span>\n      <span matTooltip=\"My Bookings\" matTooltipClass=\"tooltipred\">  \n        <a  routerLink=\"/mybookings\">\n          <li> <mat-icon class='assignment'>assignment</mat-icon></li>\n        </a>\n      </span>\n      <span matTooltip=\"Inbox\" matTooltipClass=\"tooltipred\">  \n        <a  routerLink=\"/inbox\">\n          <li> <mat-icon class='mail'>mail</mat-icon></li>\n        </a>\n      </span>\n  </ul>\n    <ul class=\"navbar-nav ml-auto\">\n        <li class=\"nav-item\">\n          <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\n            <ul class=\"navbar-nav ml-auto\">\n              <ng-template #anonymousUser>\n                <li class=\"nav-item\">\n                  <button type=\"button\" class=\"btn btn-outline-secondary mr-2\" \n                    matTooltip=\"Log In\" (click)=\"signtype='login'; openDialog()\">\n                    <mat-icon class='person' >person</mat-icon>\n                  </button>\n                  <button type=\"button\" class=\"btn btn-outline-secondary mr-2\" \n                  matTooltip=\"SignIn for New User\"  (click)=\"signtype='signup'; openDialog()\">\n                    <mat-icon class='vpn_key'>vpn_key</mat-icon>\n                  </button>\n                </li>\n              </ng-template>\n              <li ngbDropdown *ngIf=\"auth.user$ | async as user; else anonymousUser\" class=\"nav-item dropdown\">\n                <a ngbDropdownToggle class=\"nav-link dropdown-toggle\"id=\"dropdown01\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                {{ user.displayName }} \n                </a>\n                <div ngbDropdownMenu class=\"dropdown-menu show\" aria-labelledby=\"dropdown01\">\n                  <a class=\"dropdown-item\" routerLink=\"#\">My Profile</a>\n                  <a class=\"dropdown-item\" routerLink=\"#\">Manage Plan</a>\n                  <a class=\"dropdown-item\" (click)=\"logout()\">Log Out</a>\n                </div> \n              </li>\n            </ul>\n          </div>\n        </li>\n    </ul>\n\n</nav>"

/***/ }),

/***/ "./src/app/bs-navbar/bs-navbar.component.ts":
/*!**************************************************!*\
  !*** ./src/app/bs-navbar/bs-navbar.component.ts ***!
  \**************************************************/
/*! exports provided: BsNavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BsNavbarComponent", function() { return BsNavbarComponent; });
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../signup/signup.component */ "./src/app/signup/signup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BsNavbarComponent = /** @class */ (function () {
    function BsNavbarComponent(auth, dialog) {
        this.auth = auth;
        this.dialog = dialog;
        // tslint:disable-next-line:max-line-length
        this.imagePath = 'https://firebasestorage.googleapis.com/v0/b/liko-2609t.appspot.com/o/images%2FLikoLogo.png?alt=media&token=06ed98dd-64e6-4a79-b2dd-877d5df2bfa0';
    }
    BsNavbarComponent.prototype.openDialog = function () {
        if (this.signtype === 'signup') {
            this.dialog.open(_signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"]);
        }
        else {
            this.dialog.open(_login_login_component__WEBPACK_IMPORTED_MODULE_0__["LoginComponent"])
                .afterClosed()
                .subscribe(function (result) { return console.log(result); });
        }
    };
    BsNavbarComponent.prototype.logout = function () {
        this.auth.logout();
    };
    BsNavbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
            // tslint:disable-next-line:component-selector
            selector: 'bs-navbar',
            template: __webpack_require__(/*! ./bs-navbar.component.html */ "./src/app/bs-navbar/bs-navbar.component.html"),
            styles: [__webpack_require__(/*! ./bs-navbar.component.css */ "./src/app/bs-navbar/bs-navbar.component.css")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], BsNavbarComponent);
    return BsNavbarComponent;
}());



/***/ }),

/***/ "./src/app/explore/explore.component.css":
/*!***********************************************!*\
  !*** ./src/app/explore/explore.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4cGxvcmUvZXhwbG9yZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/explore/explore.component.html":
/*!************************************************!*\
  !*** ./src/app/explore/explore.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>under construction !</h3>\n<p>\n  explore page works! \n</p>\n"

/***/ }),

/***/ "./src/app/explore/explore.component.ts":
/*!**********************************************!*\
  !*** ./src/app/explore/explore.component.ts ***!
  \**********************************************/
/*! exports provided: ExploreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExploreComponent", function() { return ExploreComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExploreComponent = /** @class */ (function () {
    function ExploreComponent() {
    }
    ExploreComponent.prototype.ngOnInit = function () {
    };
    ExploreComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-explore',
            template: __webpack_require__(/*! ./explore.component.html */ "./src/app/explore/explore.component.html"),
            styles: [__webpack_require__(/*! ./explore.component.css */ "./src/app/explore/explore.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ExploreComponent);
    return ExploreComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.a {\r\n    height: 330;\r\n    \r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZOztDQUVmIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJkaXYuYSB7XHJcbiAgICBoZWlnaHQ6IDMzMDtcclxuICAgIFxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"home-page\">\n    <main class=\"mdl-layout__content\">\n        <h1 class=\"header-text\">\n          <ngb-carousel  class=\"myclass\">\n            <ng-template ngbSlide>\n              <img [src]=\"images0\" alt=\"Trusted Transport\" >\n              <div class=\"carousel-caption\">\n                <h3>BOOK FROM A TRUSTED TRANSPORT PROVIDER</h3>\n                <p>Read ratings and reviews from fellow users.</p>\n              </div> \n            </ng-template>\n            <ng-template ngbSlide>\n              <img [src]=\"images1\" alt=\"Design your Trip\" >\n              <div class=\"carousel-caption\">\n                <h3>DESIGN YOUR TRIP!</h3>\n                <p>Pick your ride and choose the one that suits your travel needs.</p>\n              </div>\n            </ng-template>\n            <ng-template ngbSlide>\n              <img [src]=\"images2\" alt=\"Book Now\" >\n              <div class=\"carousel-caption\">\n                <h3>FASTEST AND EASIEST WAY TO BOOK YOUR TRANSPORT NEEDS</h3>\n                <p>Choose, book and chat.</p>\n              </div>\n            </ng-template>\n            <ng-template ngbSlide>\n                <img [src]=\"images3\" alt=\"Download Apps\" >\n                <div class=\"carousel-caption\">\n                  <h3>Download Apps </h3>\n                  <p>Feel the experience.</p>\n                </div>\n              </ng-template>\n          </ngb-carousel>\n        </h1>\n      </main>\n    <div class=\"container-fluid\">\n      <h4>Explore Liko</h4>\n  \n    </div>\n  \n    <div class=\"container page\">\n      <div class=\"row\">\n  \n        <div class=\"col-md-9\">\n          <div class=\"feed-toggle\">\n            <ul class=\"nav nav-pills outline-active\">\n              <li class=\"nav-item\">\n                <a class=\"nav-link\">\n                   Your Feed\n                </a>\n              </li>\n              <li class=\"nav-item\">\n                <a class=\"nav-link\">\n                   Global Feed\n                </a>\n              </li>\n            </ul>\n          </div>\n  \n  \n        </div>\n  \n        <div class=\"col-md-3\">\n          <div class=\"sidebar\">\n            <p>Popular Tags</p>\n  \n  \n          </div>\n        </div>\n  \n      </div>\n    </div>\n  </div>\n  "

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.images0 = '../assets/images/car1.png';
        this.images1 = '../assets/images/car3.png';
        this.images2 = '../assets/images/car4.png';
        this.images3 = '../assets/images/car5.png';
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/inbox/inbox.component.css":
/*!*******************************************!*\
  !*** ./src/app/inbox/inbox.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luYm94L2luYm94LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/inbox/inbox.component.html":
/*!********************************************!*\
  !*** ./src/app/inbox/inbox.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>under construction !</h3>\n<p>\n  inbox works!\n</p>\n"

/***/ }),

/***/ "./src/app/inbox/inbox.component.ts":
/*!******************************************!*\
  !*** ./src/app/inbox/inbox.component.ts ***!
  \******************************************/
/*! exports provided: InboxComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboxComponent", function() { return InboxComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InboxComponent = /** @class */ (function () {
    function InboxComponent() {
    }
    InboxComponent.prototype.ngOnInit = function () {
    };
    InboxComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-inbox',
            template: __webpack_require__(/*! ./inbox.component.html */ "./src/app/inbox/inbox.component.html"),
            styles: [__webpack_require__(/*! ./inbox.component.css */ "./src/app/inbox/inbox.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], InboxComponent);
    return InboxComponent;
}());



/***/ }),

/***/ "./src/app/itenary/itenary.component.css":
/*!***********************************************!*\
  !*** ./src/app/itenary/itenary.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2l0ZW5hcnkvaXRlbmFyeS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/itenary/itenary.component.html":
/*!************************************************!*\
  !*** ./src/app/itenary/itenary.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>under construction !</h3>\n<p>\n  itenary works!\n</p>\n"

/***/ }),

/***/ "./src/app/itenary/itenary.component.ts":
/*!**********************************************!*\
  !*** ./src/app/itenary/itenary.component.ts ***!
  \**********************************************/
/*! exports provided: ItenaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItenaryComponent", function() { return ItenaryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ItenaryComponent = /** @class */ (function () {
    function ItenaryComponent() {
    }
    ItenaryComponent.prototype.ngOnInit = function () {
    };
    ItenaryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-itenary',
            template: __webpack_require__(/*! ./itenary.component.html */ "./src/app/itenary/itenary.component.html"),
            styles: [__webpack_require__(/*! ./itenary.component.css */ "./src/app/itenary/itenary.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ItenaryComponent);
    return ItenaryComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<h2>Login</h2>\n<form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\n  <div class=\"form-group\">\n      <button  (click)=\"signUpWithEmail()\" class=\"btn btn-primary\">  Sign Up with Email</button>\n  </div>\n  <div class=\"form-group\">\n      <button (click)=\"loginwithEmail()\" class=\"btn btn-primary\">  Login with Email</button>\n  </div>    \n    &nbsp;&nbsp; OR\n\n    <div class=\"form-group\">\n      <button (click)=\"loginwithGoogle()\" class=\"btn btn-primary\"> Login with Google</button>\n  </div>    \n  <div class=\"form-group\">\n      <button (click)=\"loginwithFacebook()\" class=\"btn btn-primary\"> Login with Facebook</button>\n  </div>    \n</form>\n\n \n \n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, route, router, auth) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.auth = auth;
        this.loading = false;
        this.submitted = false;
        this.email = 'email@example.com';
        this.password = 'enter password';
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        // convenience getter for easy access to form fields
        get: function () { return this.loginForm.controls; },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.loginwithGoogle = function () {
        this.auth.loginwithGoogle();
    };
    LoginComponent.prototype.loginwithFacebook = function () {
        this.auth.loginwithFacebook();
    };
    LoginComponent.prototype.signUpWithEmail = function () {
        this.auth.signUpWithEmail(this.email, this.password);
    };
    LoginComponent.prototype.loginwithEmail = function () {
        this.auth.loginwithEmail(this.email, this.password);
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_0__["AuthService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/modal/modal.component.css":
/*!*******************************************!*\
  !*** ./src/app/modal/modal.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZGFsL21vZGFsLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/modal/modal.component.html":
/*!********************************************!*\
  !*** ./src/app/modal/modal.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  modal works!\n</p>\n"

/***/ }),

/***/ "./src/app/modal/modal.component.ts":
/*!******************************************!*\
  !*** ./src/app/modal/modal.component.ts ***!
  \******************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ModalComponent = /** @class */ (function () {
    function ModalComponent() {
    }
    ModalComponent.prototype.ngOnInit = function () {
    };
    ModalComponent.prototype.ngOnDestroy = function () {
    };
    ModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! ./modal.component.html */ "./src/app/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.css */ "./src/app/modal/modal.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/mybookings/mybookings.component.css":
/*!*****************************************************!*\
  !*** ./src/app/mybookings/mybookings.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL215Ym9va2luZ3MvbXlib29raW5ncy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/mybookings/mybookings.component.html":
/*!******************************************************!*\
  !*** ./src/app/mybookings/mybookings.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>under construction !</h3>\n<p>\n  mybookings works!\n</p>\n"

/***/ }),

/***/ "./src/app/mybookings/mybookings.component.ts":
/*!****************************************************!*\
  !*** ./src/app/mybookings/mybookings.component.ts ***!
  \****************************************************/
/*! exports provided: MybookingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MybookingsComponent", function() { return MybookingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MybookingsComponent = /** @class */ (function () {
    function MybookingsComponent() {
    }
    MybookingsComponent.prototype.ngOnInit = function () {
    };
    MybookingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-mybookings',
            template: __webpack_require__(/*! ./mybookings.component.html */ "./src/app/mybookings/mybookings.component.html"),
            styles: [__webpack_require__(/*! ./mybookings.component.css */ "./src/app/mybookings/mybookings.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MybookingsComponent);
    return MybookingsComponent;
}());



/***/ }),

/***/ "./src/app/operator/bookingmanagement/bookingmanagement.component.css":
/*!****************************************************************************!*\
  !*** ./src/app/operator/bookingmanagement/bookingmanagement.component.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29wZXJhdG9yL2Jvb2tpbmdtYW5hZ2VtZW50L2Jvb2tpbmdtYW5hZ2VtZW50LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/operator/bookingmanagement/bookingmanagement.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/operator/bookingmanagement/bookingmanagement.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  bookingmanagement works!\n</p>\n"

/***/ }),

/***/ "./src/app/operator/bookingmanagement/bookingmanagement.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/operator/bookingmanagement/bookingmanagement.component.ts ***!
  \***************************************************************************/
/*! exports provided: BookingmanagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BookingmanagementComponent", function() { return BookingmanagementComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BookingmanagementComponent = /** @class */ (function () {
    function BookingmanagementComponent() {
    }
    BookingmanagementComponent.prototype.ngOnInit = function () {
    };
    BookingmanagementComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bookingmanagement',
            template: __webpack_require__(/*! ./bookingmanagement.component.html */ "./src/app/operator/bookingmanagement/bookingmanagement.component.html"),
            styles: [__webpack_require__(/*! ./bookingmanagement.component.css */ "./src/app/operator/bookingmanagement/bookingmanagement.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], BookingmanagementComponent);
    return BookingmanagementComponent;
}());



/***/ }),

/***/ "./src/app/operator/configuration/configuration.component.css":
/*!********************************************************************!*\
  !*** ./src/app/operator/configuration/configuration.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29wZXJhdG9yL2NvbmZpZ3VyYXRpb24vY29uZmlndXJhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/operator/configuration/configuration.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/operator/configuration/configuration.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  configuration works!\n</p>\n"

/***/ }),

/***/ "./src/app/operator/configuration/configuration.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/operator/configuration/configuration.component.ts ***!
  \*******************************************************************/
/*! exports provided: ConfigurationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationComponent", function() { return ConfigurationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConfigurationComponent = /** @class */ (function () {
    function ConfigurationComponent() {
    }
    ConfigurationComponent.prototype.ngOnInit = function () {
    };
    ConfigurationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-configuration',
            template: __webpack_require__(/*! ./configuration.component.html */ "./src/app/operator/configuration/configuration.component.html"),
            styles: [__webpack_require__(/*! ./configuration.component.css */ "./src/app/operator/configuration/configuration.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ConfigurationComponent);
    return ConfigurationComponent;
}());



/***/ }),

/***/ "./src/app/operator/vehicleregistration/vehicleregistration.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/operator/vehicleregistration/vehicleregistration.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL29wZXJhdG9yL3ZlaGljbGVyZWdpc3RyYXRpb24vdmVoaWNsZXJlZ2lzdHJhdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/operator/vehicleregistration/vehicleregistration.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/operator/vehicleregistration/vehicleregistration.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  vehicleregistration works!\n</p>\n"

/***/ }),

/***/ "./src/app/operator/vehicleregistration/vehicleregistration.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/operator/vehicleregistration/vehicleregistration.component.ts ***!
  \*******************************************************************************/
/*! exports provided: VehicleregistrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleregistrationComponent", function() { return VehicleregistrationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VehicleregistrationComponent = /** @class */ (function () {
    function VehicleregistrationComponent() {
    }
    VehicleregistrationComponent.prototype.ngOnInit = function () {
    };
    VehicleregistrationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vehicleregistration',
            template: __webpack_require__(/*! ./vehicleregistration.component.html */ "./src/app/operator/vehicleregistration/vehicleregistration.component.html"),
            styles: [__webpack_require__(/*! ./vehicleregistration.component.css */ "./src/app/operator/vehicleregistration/vehicleregistration.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], VehicleregistrationComponent);
    return VehicleregistrationComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.component.css":
/*!***********************************************!*\
  !*** ./src/app/profile/profile.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/profile/profile.component.html":
/*!************************************************!*\
  !*** ./src/app/profile/profile.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>under construction !</h3>\n<p>\n  profile works!\n</p>\n"

/***/ }),

/***/ "./src/app/profile/profile.component.ts":
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileComponent = /** @class */ (function () {
    function ProfileComponent() {
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = /** @class */ (function () {
    function AuthService(afAuth) {
        this.afAuth = afAuth;
        this.user$ = afAuth.authState;
    }
    AuthService.prototype.loginwithGoogle = function () {
        this.afAuth.auth.signInWithPopup(new firebase__WEBPACK_IMPORTED_MODULE_2__["auth"].GoogleAuthProvider())
            .then(function (result) {
            console.log(result);
            console.log('Login with google Successfully');
        });
    };
    AuthService.prototype.loginwithFacebook = function () {
        this.afAuth.auth.signInWithPopup(new firebase__WEBPACK_IMPORTED_MODULE_2__["auth"].FacebookAuthProvider())
            .then(function (result) {
            console.log('Login with facebook Successfully');
        });
    };
    AuthService.prototype.signUpWithEmail = function (email, password) {
        firebase__WEBPACK_IMPORTED_MODULE_2__["auth"]().createUserWithEmailAndPassword(email, password)
            .then(function (result) {
            console.log('Sign up Successfully');
        })
            .catch(function (error) {
            console.log('Error in SignUpWithEmail : ' + error);
        });
    };
    AuthService.prototype.loginwithEmail = function (email, password) {
        this.afAuth.auth.signInWithEmailAndPassword(email, password)
            .then(function (result) {
            console.log('Logged on user : ' + JSON.stringify(result));
            return true;
        })
            .catch(function (error) {
            console.log(error);
            return false;
        });
        return true;
    };
    AuthService.prototype.logout = function () {
        this.afAuth.auth.signOut()
            .then(function () {
            console.log('Sign-out successful.');
        }).catch(function (error) {
            console.log('error' + error);
        });
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_fire_auth__WEBPACK_IMPORTED_MODULE_0__["AngularFireAuth"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NpZ251cC9zaWdudXAuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  signup works!\n</p>\n<button mat-dialog-close=\"yes\" >Yes</button>"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SignupComponent = /** @class */ (function () {
    function SignupComponent() {
    }
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyCQbm4JwLPGcyE9thdAogujLKaGkDer4RA',
        authDomain: 'liko-2609m.firebaseapp.com',
        databaseURL: 'https://liko-2609m.firebaseio.com',
        projectId: 'liko-2609m',
        storageBucket: 'liko-2609m.appspot.com',
        messagingSenderId: '623370472860'
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\jwright\Documents\jwright\Practice\LikoSite\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map